package ru.dexsys.root.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.dexsys.root.dto.Customer;
import ru.dexsys.root.service.CustomerService;
import javax.servlet.http.HttpServletRequest;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/{customerId}")
    public Customer getCustomerById(@PathVariable String customerId, HttpServletRequest request) {
        Customer customer = (Customer) request.getSession().getAttribute("customer");
        return customer;
    }

    @GetMapping(value = "/firstPage")
    public ModelAndView getFirstPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("firstPage");
        return modelAndView;
    }

    @PostMapping("/firstPage")
    public ModelAndView getCustomer(HttpServletRequest request) {
        String customerId = request.getParameter("number");
        Customer customer = customerService.getCustomerDetail(customerId);
        Link link = linkTo(CustomerController.class).slash(customer.getCustomerId()).withSelfRel();
        customer.add(link);
//      Далее... Здесь может быть любая логика по формированию ссылок
        Link linkReferal = new Link("здесь может быть любая ссылка", "реферал");
        customer.add(linkReferal);
        request.getSession().setAttribute("customer", customer);
        return new ModelAndView("redirect:http://localhost:8080/" + customerId);
    }
}