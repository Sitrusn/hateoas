package ru.dexsys.root.service;

import org.springframework.stereotype.Service;
import ru.dexsys.root.dto.Customer;

@Service
public class CustomerService {

    public Customer getCustomerDetail(String customerId){
        Customer customer = new Customer();
        customer.setCustomerId(customerId);
        customer.setCustomerName("Ivan");
        customer.setCompanyName("IvanCompany");
        return customer;
    }
}