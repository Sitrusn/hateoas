package ru.dexsys.root.dto;

import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

@Data
public class Customer extends ResourceSupport {
    private String customerId;
    private String customerName;
    private String companyName;
}